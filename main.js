let infoTable = document.getElementById('info-table');
let infoForm = document.getElementById('info-form');
infoTable.style.display = 'None';
let hiddenID = document.getElementById('hid');
let contbl = document.getElementById("place-table");
let mainTable = document.getElementById('place-table');
//contbl.appendChild(createTableHeader());
let formData = {
    name:null,
    address:null,
    ratting:null,
    img_link:null
}


let db = [
    {
        id:0,
        name:'Sea Beach',
        address:"Cox's Bazar",
        ratting: 5,
        img_link: "Images/CoxBazar.jpg"
    },
    {
        id:1,
        name:'Sundarban Forest',
        address:"Khulna",
        ratting: 4,
        img_link: "Images/Sundarban.jpg"
    },
    {
        id:2,
        name:'Hill View',
        address:"Khagrachori",
        ratting: 5,
        img_link: "Images/Khagrachori.jpg"
    }
]

hiddenID.value = db.length;


function createTableHeader(){
    node = document.createElement('tr');
    node.innerHTML = `<th class="name">Name</th>
                        <th class="address">Address</th>
                        <th class="ratting">
                            Ratting
                            <select id='ratting-view'>
                                <option value="None">None</option>
                                <option value="ASC">ASC</Option>
                                <option value="DES">DES</option>
                            </select>
                        </th>
                        <th class="picture">Picture</th>
                        <th class="action">Action</th>`
    return node;
}

function createPlaceNode(val, atr){
    let node_11 = document.createElement('td');
    node_11.setAttribute('class', atr);
    node_11.innerHTML = val;
    return node_11;
}

function createPictureNode(val, atr){
    let node_11 = document.createElement('td');
    node_11.setAttribute('class', atr);
    
    let node_22 = document.createElement('img');
    node_22.setAttribute('src', val);
    node_22.setAttribute('class', 'tableImage');

    node_11.appendChild(node_22);

    return node_11;
}

function placeControler(id){
    let node = document.createElement('td');
    node.setAttribute('class', "action");

    let node_1 = document.createElement('button');
    node_1.setAttribute("class", "tableButton updateButton");
    node_1.setAttribute("id", `ubtn-${id}`);
    node_1.innerHTML = "Update";

    let node_2 = document.createElement('button');
    node_2.setAttribute("class", "tableButton deleteButton");
    node_2.setAttribute("id", `rbtn-${id}`);
    node_2.innerHTML = "Delete";

    node.appendChild(node_1);
    node.appendChild(node_2);

    return node;
}

function getPlaceNode({id, name, address, ratting, img_link}){
    let node_1 = document.createElement('tr');
    node_1.setAttribute('id', `place-${id}`);

    let node_11 = createPlaceNode(name, 'name');
    let node_12 = createPlaceNode(address, 'address');
    let node_13 = createPlaceNode(ratting, 'ratting');
    let node_14 = createPictureNode(img_link, 'picture');
    let node_15 = placeControler(id);

    node_1.appendChild(node_11);
    node_1.appendChild(node_12);
    node_1.appendChild(node_13);
    node_1.appendChild(node_14);
    node_1.appendChild(node_15);

    return node_1;
}

function displayForm2(fid = db.length){
    document.getElementById('viewer').style.display = "None";
    document.getElementById('viewerTitle').style.display = "None";
    let ff = document.getElementsByTagName('form');
    ff[0][4].value = '';
    ff[0][1].value = '';
    ff[0][2].value = '';
    ff[0][3].value = '';
    clearForm();
    infoTable.style.display = 'None';
    infoForm.style.display = 'block';
    hiddenID.value = fid;
}

function loadPlace(obj){
    let node = document.getElementById('place-table');
  //  let node_0 = createTableHeader();
    let node_1 = getPlaceNode(obj);
    node.appendChild(node_1);
}

function displayTable(){
    document.getElementById("srat").value = "None";
    document.getElementById("spl").value = null;
    tableCon();
    infoTable.style.display = 'block';
    infoForm.style.display = 'None';
    console.log('displaying Table');
    
}

function displayForm(fid = db.length){
    document.getElementById('viewer').style.display = "block";
    document.getElementById('viewerTitle').style.display = "block";
    //document.getElementById('viewer').style.display = "block";
    //document.getElementById('viewerTitle').style.display = "block";
    let ff = document.getElementsByTagName('form');
    ff[0][4].value = '';
    infoTable.style.display = 'None';
    infoForm.style.display = 'block';
    hiddenID.value = fid;
}

mainTable.addEventListener("click", function(e){
    console.log(e.path[2]);
    console.log(e.path[2].id);
    if(e.target.id.slice(0, 1) === 'r'){
        removeDBelement(parseInt(e.path[2].id.slice(6)));
        mainTable.removeChild(e.path[2]);
    }
    else if(e.target.id.slice(0, 1) === 'u'){
        let fid = parseInt(e.path[2].id.slice(6))
        let childNodes = e.path[2].childNodes;
        let fname = childNodes[0].innerHTML;
        let address = childNodes[1].innerHTML;
        let ratting = childNodes[2].innerHTML;
        let img_link = childNodes[3].childNodes[0].src;

        // new blocks of code for new reset

        formData.name = fname;
        formData.address = address;
        formData.ratting = ratting;
        formData.img_link = img_link;
        
        console.log(fid);
        console.log(fname);
        console.log(address);
        console.log(ratting);
        console.log(img_link);
        
        let ff = document.getElementsByTagName('form');

        ff[0][0].value = fid;
        ff[0][1].value = fname;
        ff[0][2].value = address;
        ff[0][3].value = ratting;
        ff[0][4].value = '';

        document.getElementById('viewer').src = img_link;
        
        displayForm(fid);
    }
}, false)


function formController(){
    if(parseInt(hiddenID.value) < db.length){
        updateFormValidate();
    }
    else{
        formValidate()
    }

}

function clearForm(){
    formData = {
        name:null,
        address:null,
        ratting:null,
        img_link:null
    }
}

function resetForm(){
    let ff = document.getElementsByTagName('form');
    ff[0][1].value = formData.name;
    ff[0][2].value = formData.address;
    ff[0][3].value = formData.ratting;
    ff[0][4].value = null;

    let errorList = ['nameError', 'addressError', 'rattingNullError', 'rattingBoundaryError', 'fileNullError', 'fileOverSizeError']

    document.getElementById('viewer').src = formData.img_link;
    errorList.forEach(e =>{
        let u = document.getElementById(e);
        u.style.display = 'None';
    })

    let boxError = ['nameBox', 'addressBox', 'rattingBox', 'fileBox']
    boxError.forEach(e =>{
        let u = document.getElementById(e);
        u.style.borderWidth = '1px';
        u.style.borderColor = 'grey';
    })
    
}

function updateFormValidate(){
    let ff = document.getElementsByTagName('form');
    let tx = parseInt(hiddenID.value);
    let idx = -1;
    for(let i = 0; i < db.length; i++){
        if(db[i].id == tx){
            idx = i;
            break;
        }
    }
    let okay = true;
    
    if(ff[0][1].value === ''){
        document.getElementById('nameBox').style.borderColor = 'red';
        document.getElementById('nameBox').style.borderWidth = '2px';
        document.getElementById('nameBox').style.borderStyle = "solid";
        document.getElementById('nameError').style.display = "block";
        // alert('Name field is empty!');
        okay = false;
    }
    
    if(ff[0][2].value === ''){
        document.getElementById('addressBox').style.borderColor = 'red';
        document.getElementById('addressBox').style.borderWidth = '2px';
        document.getElementById('addressBox').style.borderStyle = "solid";
        document.getElementById('addressError').style.display = "block";
        okay = false;
    }

    if(ff[0][3].value === ''){
        document.getElementById('rattingBox').style.borderColor = 'red';
        document.getElementById('rattingBox').style.borderWidth = '2px';
        document.getElementById('rattingBox').style.borderStyle = "solid";
        document.getElementById('rattingNullError').style.display = "block";
        okay = false;
    }

    else if(ff[0][3].value < 1 || ff[0][3].value > 5){
        document.getElementById('rattingBox').style.borderColor = 'red';
        document.getElementById('rattingBox').style.borderWidth = '2px';
        document.getElementById('rattingBox').style.borderStyle = "solid";
        document.getElementById('rattingBoundaryError').style.display = "block";
        okay = false;
    }

    if(ff[0][4].files[0]){
        if(ff[0][4].files[0].size > 10485760){
            document.getElementById('nameBox').style.borderColor = 'red';
            document.getElementById('nameBox').style.borderWidth = '2px';
            document.getElementById('nameBox').style.borderStyle = "solid";
            document.getElementById('fileOverSizeError').style.display = "block";
            //alert('file size must be within 10MB');
            okay = false;
        }else{
            db[idx].img_link = URL.createObjectURL(ff[0][4].files[0]);
        }
    }

    

    if(!okay){
        return;
    }

    

    //console.log(idx);
    //console.log(db[idx]);

    db[idx].name = ff[0][1].value;
    db[idx].address = ff[0][2].value;
    db[idx].ratting = ff[0][3].value;

    let node = document.getElementById('place-table');
    console.log(idx);
    console.log(node.childNodes[idx + 2]);
    //console.log(node.childNode[2]);
    //debugger;
    /*
    console.log(node.childNodes);
    console.log(node.childNodes[idx+2].id)
    console.log(node.childNodes[idx+2]);
    node.childNodes.forEach((child) =>{
        if(child.id === `place-${tx}`){
            node.remove()
        }
    })
    node.removeChild(node.childNodes[idx + 2]);
    let cnode = getPlaceNode(db[idx]);
    node.insertBefore(cnode, node.childNodes[idx + 2]);
    */
    displayTable();

    
}

function tableCon(){
    document.getElementById("noinfo").style.display = 'None';
    let node = document.getElementById("srat");
    if(node.value === 'DES'){
        db.sort(function(a, b){
            return b.ratting - a.ratting;
        })

    }
    else if(node.value === 'ASC'){
        db.sort(function(a, b){
            return a.ratting - b.ratting;
        })
    }
    else{
        db.sort(function(a, b){
            return a.id - b.id;
        });
    }
    sch();
}


function TableRender(tempDB){
    let node = document.getElementById('place-table');
    //console.log(node);
    //debugger;
    while(node.childNodes.length > 2){
        node.removeChild(node.childNodes[2]);
        //debugger;
    }
    tempDB.forEach(e => loadPlace(e));
}

function removeDBelement(id){
    console.log(id);
    for(let i = 0; i < db.length; i++){
        if(db[i].id == id){
            db.splice(i, 1);
            break;
        }
    }
    console.log(db);
}



function formValidate(){
    let okay = true;
    let ff = document.getElementsByTagName('form');
    if(ff[0][1].value === ''){
        document.getElementById('nameBox').style.borderColor = 'red';
        document.getElementById('nameBox').style.borderWidth = '2px';
        document.getElementById('nameBox').style.borderStyle = "solid";
        document.getElementById('nameError').style.display = "block";
        // alert('Name field is empty!');
        okay = false;
    }
    
    if(ff[0][2].value === ''){
        document.getElementById('addressBox').style.borderColor = 'red';
        document.getElementById('addressBox').style.borderWidth = '2px';
        document.getElementById('addressBox').style.borderStyle = "solid";
        document.getElementById('addressError').style.display = "block";
        okay = false;
    }

    if(ff[0][3].value === ''){
        document.getElementById('rattingBox').style.borderColor = 'red';
        document.getElementById('rattingBox').style.borderWidth = '2px';
        document.getElementById('rattingBox').style.borderStyle = "solid";
        document.getElementById('rattingNullError').style.display = "block";
        okay = false;
    }

    else if(ff[0][3].value < 1 || ff[0][3].value > 5){
        document.getElementById('rattingBox').style.borderColor = 'red';
        document.getElementById('rattingBox').style.borderWidth = '2px';
        document.getElementById('rattingBox').style.borderStyle = "solid";
        document.getElementById('rattingBoundaryError').style.display = "block";
        okay = false;
    }

    if(!ff[0][4].files[0]){
        document.getElementById('fileBox').style.borderColor = 'red';
        document.getElementById('fileBox').style.borderWidth = '2px';
        document.getElementById('fileBox').style.borderStyle = "solid";
        document.getElementById('fileNullError').style.display = "block";
        //alert('Upload a picture!');
        //return;
    }

    else if(ff[0][4].files[0].size > 10485760){
        document.getElementById('nameBox').style.borderColor = 'red';
        document.getElementById('nameBox').style.borderWidth = '2px';
        document.getElementById('nameBox').style.borderStyle = "solid";
        document.getElementById('fileOverSizeError').style.display = "block";
        //alert('file size must be within 10MB');
    }

    if(!okay){
        return;
    }

    console.log(ff[0][4].files[0]);
    console.log(ff[0][4].files[0].size);

    let obj = {
        id : ff[0][0].value,
        name : ff[0][1].value,
        address : ff[0][2].value,
        ratting : ff[0][3].value,
        img_link : URL.createObjectURL(ff[0][4].files[0])
    }

    db.push(obj);
    loadPlace(obj);
    displayTable();
    /*
    console.log(ff[0][4].value);

    let node = document.getElementById("cc");
    console.log(node.files[0].value);

    let p = document.getElementById('uuu');
    p.src = URL.createObjectURL(node.files[0]);
    */

}

function sch(){
    let x = document.getElementById("spl");
    x = x.value.toLowerCase();
    console.log(x);
    //console.log(x.value);
    ndb = db.filter((e)=>{
        let y = e.name;
        if(y.length < x.length){
            return false;
        }
        let z = y.slice(0, x.length).toLowerCase();
        
        if(x === z){
            return true;
        }
    })
    if(ndb.length == 0){
        document.getElementById("noinfo").style.display = 'block';
    }
    else{
        document.getElementById("noinfo").style.display = 'None';
    }
    TableRender(ndb);
}

function clearBoxError(obj){
    obj.style.borderWidth = "1px";
    obj.style.borderColor = "grey";
    let x = obj.id;
    console.log(x);
    if(x.slice(0, 7) === 'ratting'){
        document.getElementById('rattingNullError').style.display = 'None';
        document.getElementById('rattingBoundaryError').style.display = 'None';
    }
    else if(x.slice(0, 4) === 'file'){
        console.log('here');
        document.getElementById('fileNullError').style.display = 'None';
        document.getElementById('fileOverSizeError').style.display = 'None';
        let node = document.getElementById('fileBox');
        let node2 = document.getElementById('viewer');
        node2.src = URL.createObjectURL(node.files[0]);
        document.getElementById('viewer').style.display = "block";
        document.getElementById('viewerTitle').style.display = "block";
        //console.log(node2);

    }
    else if(x.slice(0, 4) === 'name'){
        document.getElementById('nameError').style.display = 'None';
    }
    else if(x.slice(0, 7) === 'address'){
        document.getElementById('addressError').style.display = 'None';
    }

}

